#!/bin/awk -f
{
  event = $1
  time = 0 + $2 # Make sure that "time" has a numeric type.
  node_id = $3
  pkt_size = 0 + $8
  level = $4

  if (level == "AGT" && event == "s" && $7 == "cbr") {
    sent++
    if (!startTime || (time < startTime)) {
      startTime = time
    }
  }

  if (level == "AGT" && event == "r" && $7 == "cbr") {
    receive++
    if (time > stopTime) {
      stopTime = time
    }
    recvdSize += pkt_size
  }
}

END {
  #printf("start Time = %f, stopTime = %f\n", startTime, stopTime)
  #printf("sent_packets\t %d\n",sent)
  #printf("received_packets %d\n",receive)
  #printf("PDR %.2f%\n",(receive/sent)*100);
  #printf("Average Throughput[kbps] = %.2f\tStartTime=%.2f\tStopTime = %.2f\n", (recvdSize/(stopTime-startTime))*(8/1000),startTime,stopTime);
  #printf("%.2f%\t%.2f\n", (receive/sent)*100, (recvdSize/(stopTime-startTime))*(8/1000));
  printf("%.2f\n", (recvdSize/(stopTime-startTime))*(8/1000));
}
